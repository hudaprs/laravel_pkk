<?php

// Backend Page

Route::group(['prefix' => 'dev'], function () {
	
	// Dashboard
	Route::get('/', 'HomeController@dev')->name('dev');
	// Load Item To Dashboard
	Route::get('/load-items-dashboard', 'HomeController@loadItemDashboard');


	// Auth
	Route::post('/custom/login', 'AuthController@login')->name('custom.login');
	// End Auth
	

	// Users
	Route::resource('users', 'UserController');

	// Profile
	Route::get('user/profile/{id}', 'UserController@profile')->name('user.profile');

	// Change Profile
	Route::put('/user/profile/changeProfile/{id}', 'UserController@changeProfile')->name('user.changeProfile');

	// Change Password
	Route::get('user/profile/change-password/{id}', 'UserController@changePassword')->name('user.change-password');
	Route::put('user/profile/change-password/password-change/{id}', 'UserController@passwordChange')->name('user.password-change');

	// Delete Photo Profile
	Route::put('/user/profile/deletePhotoProfile/{id}', 'UserController@deletePhotoProfile')->name('user.deletePhotoProfile');

	// Danger Zone
	// Delete Account
	Route::delete('user/delete/{id}', 'UserController@deleteAccount')->name('user.delete-account');
	// End Users

	// Items
	// Trashed Zone
	// Get All Item
	Route::get('items/trashed', 'ItemController@itemsTrashed')->name('items.trashed');
	// Restore Trashed Item
	Route::get('item/restore/{id}', 'ItemController@restore')->name('item.restore');
	// Delete Permanent Item
	Route::delete('item/delete-permanent/{id}', 'ItemController@deletePermanent')->name('item.delete-permanent');
	// End Trash Zone
	Route::resource('items', 'ItemController');
	// END Items

	//Trashed zone
	Route::get('categories/trashed', 'CategoryController@categoriesTrash')->name('categories.trashed');
	// Restore Trashed Item
	Route::get('category/restore/{id}', 'CategoryController@restore')->name('category.restore');
	// Delete Permanent Item
	Route::delete('category/delete-permanent/{id}', 'CategoryController@deletePermanent')->name('category.delete-permanent');
	// Category
	// get all category with search
	Route::get('categories/get-all', 'CategoryController@getCategories')->name('categories.get-all');

	Route::resource('categories', 'CategoryController');

	// Orders 

		// Order Index
		Route::get('/orders', 'OrderController@index')->name('orders.index');
		// Create New Order
		Route::get('/order/{id}/create-new-order', 'OrderController@create')->name('order.create');
		// Make Order
		Route::post('/order', 'OrderController@store')->name('order.store');
		// Show Order
		Route::get('/order/{id}/detail-order', 'OrderController@show')->name('order.show');
		// Edit ORder
		Route::get('/order/{id}/edit-order', 'OrderController@edit')->name('order.edit');
		// Update Order
		Route::put('/order/{id}/update-order', 'OrderController@update')->name('order.update');
		// Delete Order
		Route::delete('/order/{id}', 'OrderController@destroy')->name('order.destroy');
		// Trashed Zone
		Route::get('/orders/trashed', 'OrderController@ordersTrashed')->name('orders.index.trashed');
		// Restore Data
		Route::get('/order/{id}/restore-order', 'OrderController@restore')->name('order.restore');
		// Delete Permanent Data
		Route::delete('/order/{id}/delete-permanent', 'OrderController@deletePermanent')->name('order.delete-permanent');
	// End Orders
	
});

// End Backend

// Front End
Route::group(['prefix' => 'web'], function (){
    Route::get('/', 'PageController@index')->name('home');
});
// END Front End


Route::get('/', function () {
	return view('welcome');
});

Route::get('/', 'PageController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::match(['POST', 'GET'], '/register', function () {
	return redirect('/');
});
