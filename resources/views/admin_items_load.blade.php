{{-- Paginate --}}
<div class="pull-right" style="margin-bottom: 10px;">
  {{ $items->appends(Request::all())->links() }}
</div>

<div class="row">
    @if(count($items))		
        @foreach($items as $item)
            <div class="col-md-4">
                <div class="box box-solid box-primary">
                    <div class="box-header">
                        <div class="text-center">
                            <div class="box-title"> {{ $item->name }} </div>
                        </div>

                        <div class="text-center">
                            <img src="{{ asset('images/items_images/' . $item->image) }}" width="148px" height="128px">
                        </div>
                    </div>

                    <div class="box-body">
                        <ul>
                            <li><b>Name</b>: {{ $item->name }}</li>
                            <li><b>Code</b>: {{ $item->code }}</li>
                            <li><b>Stock</b>: {{ $item->stock  == 0 ? 'Out Of Stock' : $item->stock}}</li>
                            <li><b>Price</b>: Rp. {{ number_format($item->price) }}</li>
                            <li><b>Description</b>: {{ $item->desc }}</li>
                        </ul>

                        <small class="text-right">
                            Categories : 
                            @foreach($item->category_item as $category)
                                {{ '-' . $category->name . '-' }}
                            @endforeach
                        </small>
                    </div>

                    <div class="box-footer">
                        <a href="{{ route('order.create', $item->id) }}" class="btn btn-danger btn-block" id="btn-order">
                            <span class="fa fa-shopping-cart"></span>
                            Order Now 
                        </a>
                        <a href="#" class="btn btn-primary btn-block" onclick="return confirm('Add {{ $item->name }} To Cart ?')">
                            <span class="fa fa-plus"></span>
                            Add To Cart
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="alert alert-danger text-center text-block">
            No Items Found
        </div>
    @endif
</div>
