<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    {{--   Materialize Css   --}}
    <link rel="stylesheet" href="{{ asset('css/materialize.min.css') }}">

    {{--  Custom Css  --}}
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

    {{--  Google Fonts  --}}
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">

    <title>Minimart</title>
</head>
<body>
    {{--  Navigation  --}}
    <div class="navbar-fixed">
        <nav class="nav-wrapper blue darken-1" style="height: 80px; line-height: 80px;">
            <div class="container">
                <div class="brand-logo">
                    <a href="#sliders" class="nav-logo">MINIMART</a>
                </div>

                <a href="#" data-target="slide-out" style="margin-top: 12px;" class="sidenav-trigger">
                    <i class="material-icons">menu</i>
                </a>

                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li class="nav-item"><a href="#about" class="nav-link">About</a></li>
                    <li class="nav-item"><a href="" class="nav-link">Shop</a></li>
                    <li class="nav-item"><a href="" class="nav-link">Sign In</a></li>
                </ul>
            </div>
        </nav>
    </div>

    <ul id="slide-out" class="sidenav">
        <li class="nav-item"><a href="#about" class="nav-link">About</a></li>
        <li class="nav-item"><a href="" class="nav-link">Shop</a></li>
        <li class="nav-item"><a href="" class="nav-link">Login</a></li>
    </ul>
    {{--  END Navigation  --}}

    {{--  Image Slider  --}}
    <div class="slider scrollspy" id="sliders">
        <ul class="slides">
            <li>
                <img src="{{ asset('img/keyboardGaming.jpg') }}"> <!-- random image -->
                <div class="caption center-align">
                    <h3>This is our big Tagline!</h3>
                    <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
                </div>
            </li>
            <li>
                <img src="{{ asset('img/Lapotp.jpg') }}"> <!-- random image -->
                <div class="caption left-align">
                    <h3>Left Aligned Caption</h3>
                    <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
                </div>
            </li>
            <li>
                <img src="{{ asset('img/Laptop.jpg') }}"> <!-- random image -->
                <div class="caption right-align">
                    <h3>Right Aligned Caption</h3>
                    <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
                </div>
            </li>
        </ul>
    </div>
    {{--  END Image Slider  --}}

    {{--  Content Wrapper  --}}
    <section class="content-wrapper">
       <div id="about" class="about scrollspy" style="margin-top: 100px;">
           <div class="container">
               <div class="row">
                   <h3 class="center light">About Us</h3>

                   <div class="col m6">
                       <h4>Minimart Laravel</h4>
                       <p>Keur PKL teh aya weh gawean, eta guruna sok ngagampangkeun pisan aing mah ah Lierrrrr.
                           Cik atuh da de'ek ge loba pigawe'eun diimah. " - Kolot. Lorem ipsum dolor sit amet,
                           consectetur adipisicing elit. Assumenda facere nihil possimus quo tempore tenetur.</p>
                        <p>
                            Si ajig kukulutus
                        </p>        
                   </div>

                   <div class="col m4">
                       <img src="{{ asset('img/alfamart1.jpg') }}" width="400" alt="">
                   </div>
               </div>
           </div>
       </div>
    </section>
    {{--  END Content Wrapper  --}}
        <br>
        {{-- Section Content --}}
        
        <div class="container">
            <div class="row">
                <h3 class="light center gray-text text-darken-3">Content</h3>
                @foreach ($itemss as $item)   
                <div class="col m4 s12 center">
                    <div class="card-panel">
                        <h5>{{$item->name}}</h5>
                                <img class="materialboxed" style="margin:auto;" src="{{ asset('images/items_images/' . $item->image) }}" width="140px" height="120">
                                    <ul>
                                        <li><b>Name</b>: {{ $item->name }}</li>
                                        <li><b>Stock</b>: {{ $item->stock  == 0 ? 'Out Of Stock' : $item->stock}}</li>
                                        <li><b>Price</b>: Rp. {{ number_format($item->price) }}</li>
                                    </ul>
                                    <br>
                                    <a class="btn waves-effect waves-light blue" href="{{route('clientPages.show', ['id'=>$item->id])}}">Show
                                        <i class="material-icons right">details</i>
                                    </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
    
        {{-- End Section Content --}}        
                
    {{-- Paginate --}}
                {{-- <div class="pull-right" style="margin-bottom: 10px;">
                    {{ $itemss->appends(Request::all())->links()}}
                </div> --}}
            </section>
    {{-- End Paginate --}}
    
    


    {{--  Javascript Links  --}}
    <script src="{{ asset('js/materialize.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/jquery-3.3.1.js') }}"></script>

</body>
</html>
