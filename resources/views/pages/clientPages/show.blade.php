<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    {{--   Materialize Css   --}}
    <link rel="stylesheet" href="{{ asset('css/materialize.min.css') }}">

    {{--  Custom Css  --}}
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

    {{--  Google Fonts  --}}
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">

    <title>Minimart</title>
</head>
<body>
    {{--  Navigation  --}}
    <div class="navbar-fixed">
        <nav class="nav-wrapper blue darken-1" style="height: 80px; line-height: 80px;">
            <div class="container">
                <div class="brand-logo">
                    <a href="#sliders" class="nav-logo">MINIMART</a>
                </div>

                <a href="#" data-target="slide-out" style="margin-top: 12px;" class="sidenav-trigger">
                    <i class="material-icons">menu</i>
                </a>

                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li class="nav-item"><a href="#about" class="nav-link">About</a></li>
                    <li class="nav-item"><a href="" class="nav-link">Shop</a></li>
                    <li class="nav-item"><a href="" class="nav-link">Sign In</a></li>
                </ul>
            </div>
        </nav>
    </div>

    <ul id="slide-out" class="sidenav">
        <li class="nav-item"><a href="#about" class="nav-link">About</a></li>
        <li class="nav-item"><a href="" class="nav-link">Shop</a></li>
        <li class="nav-item"><a href="" class="nav-link">Login</a></li>
    </ul>
    {{--  END Navigation  --}}
    

    {{-- Content --}}

    
    {{--  Javascript Links  --}}
    <script src="{{ asset('js/materialize.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/jquery-3.3.1.js') }}"></script>

</body>
</html>