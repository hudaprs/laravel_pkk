@extends('layouts.global')

@section('title')
	- Update {{ $categories->name }}
@endsection

@section('page-header', 'Update Item')

@section('page-header-optional')
	Update {{ $categories->name }}
@endsection

@section('breadcumb-link')
	{{ route('categories.index') }}
	@section('breadcumb-level')
		Users
	@endsection
@endsection

@section('breadcumb-here', 'Update')
	
@section('content')
	<div class="box box-solid box-primary">
		<div class="box-header">
			<div class="box-title">
				<div class="pull-left">
					Update {{ $categories->name }}
				</div>
			</div>

			<div class="pull-right">
				<a href="{{ route('categories.index') }}" class="btn btn-danger"> Go Back</a>
			</div>
		</div>

		<div class="box-body">
            <form action="{{ route('categories.update', $categories->id) }}" method="POST" onsubmit="return confirm('Update {{ $categories->name }} ? ')" enctype="multipart/form-data">
                
				@csrf
                <input type="hidden" name="_method" value="PUT">
                
			<div class="row">
				<div class="col-md-6">
					<div class="form-group {{ $errors->first('name') ? 'has-error' : '' }}">
						<label for="name">Name</label>
						<input type="text" name="name" id="name" class="form-control" placeholder="Name" autocomplete="off" value="{{ $categories->name }}">

						<div class="help-block">
							{{ $errors->first('name') }}
						</div>
					</div>
					
			</div>
		</div>

		<div class="box-footer">
			<button type="submit" name="submit" class="btn btn-primary pull-right">
				Update Item
			</button>
		</div>
			</form>				
	</div>	
@endsection