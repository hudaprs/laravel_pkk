@extends('layouts.global')

@section('title', '- Category Management')

@section('page-header', 'Category Management')

@section('page-header-optional', 'Manage Your Items Here')

@section('breadcumb-link')
	{{ route('categories.index') }}
	@section('breadcumb-level')
		Users
	@endsection
@endsection

@section('breadcumb-here', 'Dashboard')

@section('content')
	<div class="box box-solid box-primary">
		<div class="box-header">
			<div class="box-title">
				<div class="pull-left">
				Category List
				</div>
			</div>

			<div class="pull-right">
				<a href="{{ route('categories.create') }}" class="btn btn-success"><span class="fa fa-plus"> </span> Create New Category</a>
				<a href="{{ route('categories.trashed') }}" class="btn btn-danger"><span class="fa fa-trash"> </span> Trash Category</a>
			</div>
		</div>

		<div class="box-body">
			<div class="pull-right">
			</div>

			<div class="table table-responsive">
				<table class="table table-striped" id="datatable">
					<thead>
						<tr>
                            <th>#</th>
							<th>Name</th>
							<th>Code</th>
							<th> Action </th>
						</tr>
					</thead>

					<tbody>
						@php $no = 1 @endphp
						@foreach ($categories as $category)
							<tr>
								<td>{{ $no }}.</td>
								<td>{{ $category->name }}</td>
								<td>{{ $category->code }}</td>
								<td>
									<a href="{{ route('categories.show', $category->id) }}" class="btn btn-primary btn-sm pull-left" style="margin-left: 10px">
										<span class="fa fa-eye"></span>
									</a>
									<a href="{{ route('categories.edit', $category->id) }}" class="btn btn-warning btn-sm pull-left" style="margin-left: 10px">
										<span class="fa fa-edit"></span>
									</a>
									<form action="{{ route('categories.destroy', $category->id) }}" method="POST" onsubmit="return confirm('Delete {{ $category->name }} ? ')" class="pull-left" style="margin-left: 10px">
										@csrf
										<input type="hidden" name="_method" value="DELETE">
										<button type="submit" class="btn btn-danger btn-sm">
											<span class="fa fa-trash"></span>
										</button>
									</form>
								</td>
							</tr>
						@php $no++ @endphp
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@push('script')
	<script>
		$(function (){
			$('#datatable').DataTable();
		});
	</script>
@endpush