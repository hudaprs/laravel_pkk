@extends('layouts.global')

@section('title', '- Category Management')

@section('page-header', 'Category Management')

@section('page-header-optional', 'Create New Category')

@section('breadcumb-link')
	{{ route('categories.index') }}
	@section('breadcumb-level')
		Category
	@endsection
@endsection

@section('breadcumb-here', 'Create')
	
@section('content')
	<div class="box box-solid box-primary">
		<div class="box-header">
			<div class="box-title">
				<div class="pull-left">
					Create New Category
				</div>
			</div>

			<div class="pull-right">
				<a href="{{ route('categories.index') }}" class="btn btn-danger"> Go Back</a>
			</div>
		</div>

		<div class="box-body">
			<form action="{{ route('categories.store') }}" method="POST" onsubmit="return confirm('Create New Category ? ')" enctype="multipart/form-data">
				@csrf
			<div class="row">
				<div class="col-md-6">
					<div class="form-group {{ $errors->first('name') ? 'has-error' : '' }}">
						<label for="name">Name</label>
						<input type="text" name="name" id="name" class="form-control" placeholder="Name" autocomplete="off" value="{{ old('name') }}">

						<div class="help-block">
							{{ $errors->first('name') }}
						</div>
					</div>
					</div>
				</div>
            </div>
            <div class="box-footer">
                <button type="submit" name="submit" class="btn btn-primary pull-right">
                    Create Category
                </button>
            </div>
                </form>		
			</div>
@endsection