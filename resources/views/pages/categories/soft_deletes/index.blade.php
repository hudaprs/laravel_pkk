@extends('layouts.global')

@section('title', '- Trashed Category Management')

@section('page-header', 'Trashed Category Management')

@section('page-header-optional', 'Manage Your Trashed Category Here')

@section('breadcumb-link')
	{{ route('categories.trashed') }}
	@section('breadcumb-level')
		Trashed Category
	@endsection
@endsection

@section('breadcumb-here', 'Dashboard')

@section('content')
	<div class="box box-solid box-danger">
		<div class="box-header">
			<div class="box-title">
				<div class="pull-left">
				Trashed Category List
				</div>
			</div>

			<div class="pull-right">
				<a href="{{ route('categories.index') }}" class="btn btn-success"><span class="fa fa-check"> </span> Untrashed Category</a>
			</div>
		</div>

		<div class="box-body">
			<div class="pull-right">
			</div>

			<div class="table table-responsive">
				<table class="table table-striped" id="datatable">
					<thead>
						<tr>
                            <th>#</th>
							<th>Name</th>
							<th>Code</th>
						</tr>
					</thead>

					<tbody>
						@php $no = 1 @endphp
						@foreach ($categories as $category)
							<tr>
								<td>{{ $no }}.</td>
								<td>{{ $category->name }}</td>
								<td>{{ $category->code }}</td>
									{{-- Restore --}}
									<a href="{{ route('category.restore', $category->id) }}" class="btn btn-success btn-sm pull-left" style="margin-left: 10px" onclick="return confirm('Restore {{ $category->name }}')">
										<span class="fa fa-check"></span>
									</a>

									{{-- Delete Permanent --}}
									<form action="{{ route('category.delete-permanent', $category->id) }}" method="POST" onsubmit="return confirm('Delete Permanent {{ $category->name }} ? ')" class="pull-left" style="margin-left: 10px">
										@csrf
										<input type="hidden" name="_method" value="DELETE">
										<button type="submit" class="btn btn-danger btn-sm">
											<span class="fa fa-trash"></span>
										</button>
									</form>
								</td>
							</tr>
						@php $no++ @endphp
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@push('script')
	<script>
		$(function (){
			$('#datatable').DataTable();
		});
	</script>
@endpush