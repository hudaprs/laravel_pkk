@extends('layouts.global')

@section('title')
	- Create New Order
@endsection

@section('page-header', 'Create New Order')

@section('page-header-optional')
	Create New Order Here
@endsection

@section('breadcumb-link')
	{{ route('orders.index') }}
	@section('breadcumb-level')
		Orders
	@endsection
@endsection

@section('breadcumb-here', 'Update')
	
@section('content')
	<div class="box box-solid box-primary">
		<div class="box-header">
			<div class="box-title">
				<div class="pull-left">
          Order {{ $item->name }} - {{ $item->code }}
				</div>
			</div>

			<div class="pull-right">
				<a href="{{ route('dev') }}" class="btn btn-danger"> Go Back</a>
			</div>
		</div>

		<div class="box-body">
			<form action="{{ route('order.store') }}" method="POST" onsubmit="return confirm('Order {{ $item->name }} ? ')">
				@csrf
                <input type="hidden" name="item_id" value="{{ $item->id }}">
                
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->first('name') ? 'has-error' : '' }}">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Name" autocomplete="off" value="{{ $item->name }}" readonly>

                        <div class="help-block">
                            {{ $errors->first('name') }}
                        </div>
                    </div>
                    
                    <div class="form-group {{ $errors->first('desc') ? 'has-error' : '' }}">
                        <label for="desc">Desc</label>
                        <input type="desc" name="desc" id="desc" class="form-control" placeholder="desc" autocomplete="off" value="{{ $item->desc }}" readonly>

                        <div class="help-block">
                            {{ $errors->first('desc') }}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->first('stock') ? 'has-error' : '' }}">
                        <label for="stock">Stock</label>
                        <input type="number" name="stock" id="stock" class="form-control" value="{{ $item->stock }}" readonly>
                        <div class="help-block">
                            {{ $errors->first('stock') }}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->first('price') ? 'has-error' : '' }}">
                        <label for="price">Price</label>
                        <input type="number" name="price" id="price" class="form-control" value="{{ $item->price }}" readonly>

                        <div class="help-block">
                            {{ $errors->first('price') }}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->first('quantity') ? 'has-error' : '' }}">
                        <label for="quantity">Quantity</label>
                        <input type="number" name="quantity" id="quantity" class="form-control" value="{{ old('quantity') }}" placeholder="Quantity">

                        <div class="help-block">
                            {{ $errors->first('quantity') }}
                        </div>
                    </div>

                    <div class="form-group total-price">
                        <label for="total_price">Total Price</label>
                        <input type="number" name="total_price" id="total_price" class="form-control" placeholder="Total Price" readonly>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">

                        <img src="{{ asset('images/items_images/' .$item->image) }}" width="70%" style="margin-top: 10px;">
                    </div>
                </div>
            </div>
		</div>

		<div class="box-footer">
			<button type="submit" name="submit" class="btn btn-primary pull-right" id="btn-create-order">
				Create Order
      </button>
		</div>
			</form>				
	</div>	
@endsection

@push('script')
    <script>
        $(function() {
          $('.total-price').hide() 
          $('#btn-create-order').show()

          $('#quantity').on('keyup', function() {
            let qty = $(this).val()
            let sum = qty * $('#price').val()

            // Sum Quantity
            if(qty) {
              $('.total-price').fadeIn() 
              $('#total_price').val(sum)
            }else {
              $('.total-price').fadeOut()
            }
          })
        })
    </script>
@endpush