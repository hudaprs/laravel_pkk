@extends('layouts.global')

@section('title')
  - Update Order
@endsection

@section('page-header', 'Update Order')

@section('page-header-optional')
  Update Costumer Order
@endsection

@section('breadcumb-link')
  {{ route('orders.index') }}
  @section('breadcumb-level')
    Orders
  @endsection
@endsection

@section('breadcumb-here', 'Update')
  
@section('content')
  <div class="box box-solid box-primary">
    <div class="box-header">
      <div class="box-title">
        <div class="pull-left">
          Order {{ $order->item_ordered->name }} - {{ $order->item_ordered->code }}
        </div>
      </div>

      <div class="pull-right">
        <a href="{{ route('orders.index') }}" class="btn btn-danger"> Go Back</a>
      </div>
    </div>

    <div class="box-body">
      <form action="{{ route('order.update', $order->id) }}" method="POST" onsubmit="return confirm('Update {{ $order->invoice }} ? ')">
        @csrf
            <input type="hidden" name="_method" value="PUT">
                
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-striped">
                      <tr>
                        <th>Invoice</th>
                        <th>:</th>
                        <td><b>{{ $order->invoice }}</b></td>
                      </tr>

                      <tr>
                        <th>Ordered By</th>
                        <th>:</th>
                        <td><b>{{ $order->user_create_order->name }}</b></td>
                      </tr>

                      <tr>
                        <th>Ordering at </th>
                        <th>:</th>
                        <td><b>{{ date('m-d-Y', strtotime($order->created_at)) }}</b></td>
                      </tr>

                      <tr>
                        <th>Address</th>
                        <th>:</th>
                        <th><b>{{ $order->user_create_order->address }}</b></th>
                      </tr>

                      <tr>
                        <th>Name</th>
                        <th>:</th>
                        <td><b>{{ $order->item_ordered->name }}</b></td>
                      </tr>

                      <tr>
                        <th>Item Price</th>
                        <th>:</th>
                        <td><b>Rp. {{ number_format($order->item_ordered->price) }}</b></td>
                      </tr>

                      <tr>
                        <th>Quantity Ordered</th>
                        <th>:</th>
                        <th><b>{{ $order->quantity }}</b></th>
                      </tr>

                      <tr>
                        <th>Fee</th>
                        <th>:</th>
                        <td><b>Rp. {{ $order->fee }}</b></td>
                      </tr>

                      <tr>
                        <th>Total Price</th>
                        <th>:</th>
                        <th><b>Rp. {{ number_format($order->total_price) }}</b></th>
                      </tr>

                      <tr>
                        <th>Status</th>
                        <th>:</th>
                        <th>
                          @if($order->status === "PENDING")
                              <div class="label label-info">
                                  {{ $order->status }}
                              </div>
                          @elseif($order->status === "DELIVERED")
                              <div class="label label-warning">
                                  {{ $order->status }}
                              </div>
                          @elseif($order->status === "PROCESS")
                              <div class="label label-primary">
                                  {{ $order->status }}
                              </div>
                          @elseif($order->status === "SUCCESS")
                              <div class="label label-success">
                                  {{ $order->status }}
                              </div>
                          @else   
                              <div class="label label-danger">
                                  No Status
                              </div>
                          @endif
                        </th>
                      </tr>
                    </table>


                    <div class="form-group {{ $errors->first('status') ? 'has-error' : '' }}">
                      <label for="status">Update Status</label>
                      @php $statuses = ['PROCESS', 'PENDING', 'DELIVERED', 'SUCCESS'] @endphp
                        <select name="status" id="status" class="form-control">
                          <option value="">Choose Status</option>
                          @foreach($statuses as $status)
                            @if($order->status == $status)
                              <option value="{{ $status }}" selected>{{ $status }}</option>
                            @else
                              <option value="{{ $status }}">{{ $status }}</option>
                            @endif
                          @endforeach
                        </select>

                        <div class="help-block">
                          {{ $errors->first('status') }}
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <img src="{{ asset('images/items_images/' . $order->item_ordered->image) }}" width="70%" style="margin-top: 10px;">
                    </div>
                </div>
            </div>
    </div>

    <div class="box-footer">
      <button type="submit" name="submit" class="btn btn-primary pull-right" id="btn-create-order">
        Update Order
      </button>
    </div>
      </form>       
  </div>  
@endsection