@extends('layouts.global')

@section('title', '- Order Management')

@section('page-header', 'Order Management')

@section('page-header-optional', 'Manage Your Orders Here')

@section('breadcumb-link')
	{{ route('orders.index') }}
	@section('breadcumb-level')
		Orders
	@endsection
@endsection

@section('breadcumb-here', 'Dashboard')

@section('content')
	<div class="box box-solid box-primary">
		<div class="box-header">
			<div class="box-title">
				<div class="pull-left">
				 	Order List
				</div>
			</div>

      <div class="pull-right">
        <a href="{{ route('orders.index.trashed') }}" class="btn btn-danger">
          <span class="fa fa-trash"></span>
          Trashed Orders
        </a>
      </div>
		</div>

		<div class="box-body">
			<div class="table table-responsive">
				<table class="table table-striped" id="datatable">
					<thead>
						<tr>
							<th>#</th>
							<th>Invoice</th>
							<th>Order By</th>
							<th>Item</th>
							<th>Quantity</th>
							<th>Total Price</th>
							<th>Fee</th>
							<th>Status</th>
              <th>Responded By</th>
              <th></th>
						</tr>
					</thead>

					<tbody>
						@php $no = 1 @endphp
						@foreach($orders as $order)
							<tr>
								<td>{{ $no++ }}</td>
								<td>{{ $order->invoice }}</td>
								<td>{{ $order->user_create_order->name }}</td>
								<td>{{ $order->item_ordered->name }}</td>
								<td>{{ $order->quantity }}</td>
								<td>Rp. {{ number_format($order->total_price) }}</td>
								<td>Rp. {{ number_format($order->fee) }}</td>
                <td>
                    @if($order->status === "PENDING")
                        <div class="label label-info">
                            {{ $order->status }}
                        </div>
                    @elseif($order->status === "DELIVERED")
                        <div class="label label-warning">
                            {{ $order->status }}
                        </div>
                    @elseif($order->status === "PROCESS")
                        <div class="label label-primary">
                            {{ $order->status }}
                        </div>
                    @elseif($order->status === "SUCCESS")
                        <div class="label label-success">
                            {{ $order->status }}
                        </div>
                    @else   
                        <div class="label label-danger">
                            No Status
                        </div>
                    @endif
								</td>
								<td>{{ $order->responded_by !== null ? $order->user_response_order->name : 'No Response Yet'}}</td>
								<td>
									<a href="{{ route('order.show', $order->id) }}" class="btn btn-primary btn-sm pull-left" style="margin-left: 10px">
										<span class="fa fa-eye"></span>
									</a>
									<a href="{{ route('order.edit', $order->id) }}" class="btn btn-warning btn-sm pull-left" style="margin-left: 10px">
										<span class="fa fa-edit"></span>
									</a>
									<form action="{{ route('order.destroy', $order->id) }}" method="POST" onsubmit="return confirm('Delete {{ $order->invoice }} ? ')" class="pull-left" style="margin-left: 10px">
										@csrf
										<input type="hidden" name="_method" value="DELETE">
										<button type="submit" class="btn btn-danger btn-sm">
											<span class="fa fa-trash"></span>
										</button>
									</form>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@push('script')
	<script>
		$(function() {
		  $("#datatable").DataTable()
		})
	</script>
@endpush
