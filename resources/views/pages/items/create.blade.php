@extends('layouts.global')

@section('title', '- Item Management')

@section('page-header', 'Item Management')

@section('page-header-optional', 'Create New Item')

@section('breadcumb-link')
	{{ route('items.index') }}
	@section('breadcumb-level')
		Items
	@endsection
@endsection

@section('breadcumb-here', 'Create')

@section('content')
	<div class="box box-solid box-primary">
		<div class="box-header">
			<div class="box-title">
				<div class="pull-left">
					Create New Item
				</div>
			</div>

			<div class="pull-right">
				<a href="{{ route('items.index') }}" class="btn btn-danger"> Go Back</a>
			</div>
		</div>

		<div class="box-body">
			<form action="{{ route('items.store') }}" method="POST" onsubmit="return confirm('Create New Item ? ')" enctype="multipart/form-data">
				@csrf
			<div class="row">
				<div class="col-md-6">
					<div class="form-group {{ $errors->first('name') ? 'has-error' : '' }}">
						<label for="name">Name</label>
						<input type="text" name="name" id="name" class="form-control" placeholder="Name" autocomplete="off" value="{{ old('name') }}">

						<div class="help-block">
							{{ $errors->first('name') }}
						</div>
					</div>

					<div class="form-group {{ $errors->first('') ? 'has-error' : '' }}">
						<label for="categories">Attach Categories For Item</label>
						<select name="categories[]" id="categories" class="form-control" style="width:100%" multiple>

						</select>
					</div>

					<div class="form-group {{ $errors->first('desc') ? 'has-error' : '' }}">
						<label for="desc">Description</label>

                        <textarea name="desc" id="desc" value="{{ old('desc') }}" class="form-control" cols="30" rows="10">
                            {{ old('desc') }}
                        </textarea>

						<div class="help-block">
							{{ $errors->first('desc') }}
						</div>
					</div>

					<div class="form-group {{ $errors->first('stock') ? 'has-error' : '' }}">
						<label for="stock">Stock</label>
						<input type="number" name="stock" id="stock" class="form-control" placeholder="Stock" autocomplete="off" value="{{ old('stock') }}">

						<div class="help-block">
							{{ $errors->first('stock') }}
						</div>
                    </div>

					<div class="form-group {{ $errors->first('price') ? 'has-error' : '' }}">
						<label for="price">price</label>
						<input type="number" name="price" id="price" class="form-control" value="{{ old('price') }}" placeholder="Price">

						<div class="help-block">
							{{ $errors->first('price') }}
						</div>
					</div>

				</div>

                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" name="image" id="image">
                </div>
			</div>
		</div>
        <div class="box-footer">
            <button type="submit" name="submit" class="btn btn-primary pull-right">
                Create User
            </button>
        </div>
			</form>
@endsection


@push('script')
<script>
    // Via ServerSide
    $(function() {
        $('#categories').select2({
			placeholder: 'Choose Categories',
			ajax : {
				url : '{{ route("categories.get-all") }}',
				processResults : function(data){
					return {
						results : data.map(function(category){
							return {id:category.id, text:category.name}
						})
					}
				}
			}
		});
    });
</script>
@endpush
