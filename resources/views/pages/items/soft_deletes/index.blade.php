@extends('layouts.global')

@section('title', '- Trashed Item Management')

@section('page-header', 'Trashed Item Management')

@section('page-header-optional', 'Manage Your Trashed Items Here')

@section('breadcumb-link')
	{{ route('items.trashed') }}
	@section('breadcumb-level')
		Trashed Items
	@endsection
@endsection

@section('breadcumb-here', 'Dashboard')

@section('content')
	<div class="box box-solid box-danger">
		<div class="box-header">
			<div class="box-title">
				<div class="pull-left">
				Trashed Item List
				</div>
			</div>

			<div class="pull-right">
				<a href="{{ route('items.index') }}" class="btn btn-success"><span class="fa fa-check"> </span> Untrashed Items</a>
			</div>
		</div>

		<div class="box-body">
			<div class="pull-right">
			</div>

			<div class="table table-responsive">
				<table class="table table-striped" id="datatable">
					<thead>
						<tr>
                            <th>#</th>
							<th>Image</th>
							<th>Name</th>
							<th>Code</th>
							<th>Stock</th>
							<th>Price</th>
							<th> Action </th>
						</tr>
					</thead>

					<tbody>
						@php $no = 1 @endphp
						@foreach ($items as $item)
							<tr>
								<td>{{ $no }}.</td>
								<td>
									<img src="{{ asset('images/items_images/' . $item->image) }}" alt="images" srcset="" width="48px">
								</td>
								<td>{{ $item->name }}</td>
								<td>{{ $item->code }}</td>
								<td>{{ $item->stock }}</td>
								<td>Rp. {{ number_format($item->price) }}td>
								<td>
									{{-- Restore --}}
									<a href="{{ route('item.restore', $item->id) }}" class="btn btn-success btn-sm pull-left" style="margin-left: 10px" onclick="return confirm('Restore {{ $item->name }}')">
										<span class="fa fa-check"></span>
									</a>

									{{-- Delete Permanent --}}
									<form action="{{ route('item.delete-permanent', $item->id) }}" method="POST" onsubmit="return confirm('Delete Permanent {{ $item->name }} ? ')" class="pull-left" style="margin-left: 10px">
										@csrf
										<input type="hidden" name="_method" value="DELETE">
										<button type="submit" class="btn btn-danger btn-sm">
											<span class="fa fa-trash"></span>
										</button>
									</form>
								</td>
							</tr>
						@php $no++ @endphp
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@push('script')
	<script>
		$(function (){
			$('#datatable').DataTable();
		});
	</script>
@endpush