@extends('layouts.global')

@section('title', '- Item Management')

@section('page-header', 'Item Management')

@section('page-header-optional', 'Manage Your Items Here')

@section('breadcumb-link')
	{{ route('items.index') }}
	@section('breadcumb-level')
		Users
	@endsection
@endsection

@section('breadcumb-here', 'Dashboard')

@section('content')
	<div class="box box-solid box-primary">
		<div class="box-header">
			<div class="box-title">
				<div class="pull-left">
				Item List
				</div>
			</div>

			<div class="pull-right">
				<a href="{{ route('items.create') }}" class="btn btn-success"><span class="fa fa-plus"> </span> Create New Items</a>
				<a href="{{ route('items.trashed') }}" class="btn btn-danger"><span class="fa fa-trash"></span>Trashed Items</a>
			</div>
		</div>

		<div class="box-body">
			<div class="pull-right">
			</div>

			<div class="table table-responsive">
				<table class="table table-striped" id="datatable">
					<thead>
						<tr>
                            <th>#</th>
							<th>Image</th>
							<th>Name</th>
							<th>Code</th>
							<th>Stock</th>
							<th>Price</th>
							<th> Action </th>
						</tr>
					</thead>

					<tbody>
						@php $no = 1 @endphp
						@foreach ($items as $item)
							<tr>
								<td>{{ $no }}.</td>
								<td>
									<img src="{{ asset('images/items_images/' . $item->image) }}" alt="images" srcset="" width="48px">
								</td>
								<td>{{ $item->name }}</td>
								<td>{{ $item->code }}</td>
								<td>{{ $item->stock }}</td>
								<td>Rp. {{ number_format($item->price) }}</td>
								<td>
									<a href="{{ route('items.show', $item->id) }}" class="btn btn-primary btn-sm pull-left" style="margin-left: 10px">
										<span class="fa fa-eye"></span>
									</a>
									<a href="{{ route('items.edit', $item->id) }}" class="btn btn-warning btn-sm pull-left" style="margin-left: 10px">
										<span class="fa fa-edit"></span>
									</a>
									<form action="{{ route('items.destroy', $item->id) }}" method="POST" onsubmit="return confirm('Delete {{ $item->name }} ? ')" class="pull-left" style="margin-left: 10px">
										@csrf
										<input type="hidden" name="_method" value="DELETE">
										<button type="submit" class="btn btn-danger btn-sm">
											<span class="fa fa-trash"></span>
										</button>
									</form>
								</td>
							</tr>
						@php $no++ @endphp
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@push('script')
	<script>
		$(function (){
			$('#datatable').DataTable();
		});
	</script>
@endpush