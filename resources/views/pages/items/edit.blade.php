@extends('layouts.global')

@section('title')
	- Update {{ $item->name }}
@endsection

@section('page-header', 'Update Item')

@section('page-header-optional')
	Update {{ $item->name }}
@endsection

@section('breadcumb-link')
	{{ route('items.index') }}
	@section('breadcumb-level')
		Users
	@endsection
@endsection

@section('breadcumb-here', 'Update')
	
@section('content')
	<div class="box box-solid box-primary">
		<div class="box-header">
			<div class="box-title">
				<div class="pull-left">
					Update {{ $item->name }}
				</div>
			</div>

			<div class="pull-right">
				<a href="{{ route('items.index') }}" class="btn btn-danger"> Go Back</a>
			</div>
		</div>

		<div class="box-body">
            <form action="{{ route('items.update', $item->id) }}" method="POST" onsubmit="return confirm('Update {{ $item->name }} ? ')" enctype="multipart/form-data">
                
				@csrf
                <input type="hidden" name="_method" value="PUT">
                
			<div class="row">
				<div class="col-md-6">
					<div class="form-group {{ $errors->first('name') ? 'has-error' : '' }}">
						<label for="name">Name</label>
						<input type="text" name="name" id="name" class="form-control" placeholder="Name" autocomplete="off" value="{{ $item->name }}">

						<div class="help-block">
							{{ $errors->first('name') }}
						</div>
					</div>

					<div class="form-group {{ $errors->first('') ? 'has-error' : '' }}">
						<label for="categories">Attach Categories For Item</label>
						<select name="categories[]" id="categories" class="form-control" style="width:100%" multiple>
							
						</select>
					</div>
					
					<div class="form-group {{ $errors->first('desc') ? 'has-error' : '' }}">
						<label for="desc">Desc</label>
						<input type="desc" name="desc" id="desc" class="form-control" placeholder="desc" autocomplete="off" value="{{ $item->desc }}">

						<div class="help-block">
							{{ $errors->first('desc') }}
						</div>
					</div>

					<div class="form-group {{ $errors->first('stock') ? 'has-error' : '' }}">
						<label for="stock">Stock</label>
						<input type="number" name="stock" id="stock" class="form-control" value="{{ $item->stock }}">
						<div class="help-block">
							{{ $errors->first('stock') }}
						</div>
					</div>

					<div class="form-group {{ $errors->first('price') ? 'has-error' : '' }}">
						<label for="price">Price</label>
						<input type="number" name="price" id="price" class="form-control" value="{{ $item->price }}">

						<div class="help-block">
							{{ $errors->first('price') }}
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label for="image">Image</label>
						<input type="file" name="image" id="image">

						<img src="{{ asset('images/items_images/' .$item->image) }}" width="100" style="margin-top: 10px;" alt="" srcset="">
					</div>
				</div>
			</div>
		</div>

		<div class="box-footer">
			<button type="submit" name="submit" class="btn btn-primary pull-right">
				Update Item
			</button>
		</div>
			</form>				
	</div>	
@endsection

@push('script')
	<script>
		$(function() {
			$('#categories').select2({
				placeholder: 'Choose Categories',
				ajax : {
					url : '{{ route("categories.get-all") }}',
					processResults : function(data){
						return {
							results : data.map(function(category){
								return {id:category.id, text:category.name}
							})
						}
					}
				}
			});

			var categories = {!! $item->category_item !!}
            categories.forEach(function(category){
                var option = new Option(category.name, category.id, true, true);
                $('#categories').append(option).trigger('change');
            });
		})
	</script>
@endpush