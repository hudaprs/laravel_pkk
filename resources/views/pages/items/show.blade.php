@extends('layouts.global')

@section('title')
	- {{ $item->name }}
@endsection

@section('page-header', 'Detail Item')

@section('page-header-optional')
	Detail {{ $item->name }}
@endsection

@section('breadcumb-link')
	{{ route('items.index') }}
	@section('breadcumb-level')
		Items
	@endsection
@endsection

@section('breadcumb-here', 'Show')

@section('content')
	<div class="box box-solid box-primary">
		<div class="box-header">
			<div class="box-title">
				<div class="pull-left">
					Detail {{ $item->name }}
				</div>
			</div>

			<div class="pull-right">
				<a href="{{ route('items.index') }}" class="btn btn-danger"> Go Back</a>
			</div>
		</div>

		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="name">Name</label>
						<input type="text" name="name" id="name" class="form-control" placeholder="Name" autocomplete="off" value="{{ $item->name }}" readonly>
					</div>
					
					<div class="form-group">
						<label for="code">Code</label>
						<input type="text" name="code" id="code" class="form-control" placeholder="Code" autocomplete="off" value="{{ $item->code }}" readonly>
					</div>

					<div class="form-group">
						<label for="desc">Description</label>
						<textarea id="desc" class="form-control" placeholder="desc" autocomplete="off" readonly> {{ $item->desc }}</textarea>
					</div>

					<div class="form-group">
						<label for="price">Price</label>
						<input type="number" name="price" id="price" class="form-control" value="{{ $item->price }}" readonly>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
                        <label for="stock">Stock</label>
                        <input type="text" name="stock" id="stock" class="form-control" value="{{ $item->stock }}" readonly>
                    </div>

                    <div class="form-group">
                        <img src="{{ asset('images/items_images/' .$item->image) }}" width="200" alt="">
                    </div>
				</div>
			</div>
		</div>		
	</div>	
@endsection	