@extends('layouts.global')

@section('title', '- User Management')

@section('page-header', 'User Management')

@section('page-header-optional', 'Manage Your Users Here')

@section('breadcumb-link')
	{{ route('users.index') }}
	@section('breadcumb-level')
		Users
	@endsection
@endsection

@section('breadcumb-here', 'Dashboard')

@section('content')
	<div class="box box-solid box-primary">
		<div class="box-header">
			<div class="box-title">
				<div class="pull-left">
				 	User List
				</div>
			</div>

			<div class="pull-right">
				<a href="{{ route('users.create') }}" class="btn btn-success"><span class="fa fa-plus"></span> Create New User</a>
			</div>
		</div>

		<div class="box-body">
			<div class="pull-right">
			</div>

			<div class="table table-responsive">
				<table class="table table-striped" id="datatable">
					<thead>
						<tr>
							<th>#</th>
							<th>Image</th>
							<th>Name</th>
							<th>Username</th>
							<th>Email</th>
							<th>Level</th>
							<th>Status</th>
							<th></th>
						</tr>
					</thead>

					<tbody>
						@php $no = 1 @endphp
						@foreach($users as $user)
							<tr>
								<td>{{ $no++ }}</td>
								<td>
									<img src="{{ asset('images/users_images/' . $user->image) }}" width="48px">
								</td>
								<td>{{ $user->name }}</td>
								<td>{{ $user->username }}</td>
								<td>{{ $user->email }}</td>
								<td>
									<div class="{{ $user->level == 'ADMIN' ? 'label label-success' : '' }}">
										{{ $user->level }}
									</div>
								</td>
								<td>{{ $user->status }}</td>
								<td>
									<a href="{{ route('users.show', $user->id) }}" class="btn btn-primary btn-sm pull-left" style="margin-left: 10px">
										<span class="fa fa-eye"></span>
									</a>
									<a href="{{ route('users.edit', $user->id) }}" class="btn btn-warning btn-sm pull-left" style="margin-left: 10px">
										<span class="fa fa-edit"></span>
									</a>
									<form action="{{ route('users.destroy', $user->id) }}" method="POST" onsubmit="return confirm('Delete {{ $user->name }} ? ')" class="pull-left" style="margin-left: 10px">
										@csrf
										<input type="hidden" name="_method" value="DELETE">
										<button type="submit" class="btn btn-danger btn-sm">
											<span class="fa fa-trash"></span>
										</button>
									</form>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			{{-- Pagination --}}
			<div class="box-footer pull-right">
				{{ $users->appends(Request::all())->links() }}
			</div>
		</div>
	</div>
@endsection

@push('script')
	<script>
		$(function() {
			$('#datatable').DataTable()
		})
	</script>
@endpush