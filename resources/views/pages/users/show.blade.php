@extends('layouts.global')

@section('title')
	- {{ $user->name }}
@endsection

@section('page-header', 'Detail User')

@section('page-header-optional')
	Detail {{ $user->name }}
@endsection

@section('breadcumb-link')
	{{ route('users.index') }}
	@section('breadcumb-level')
		Users
	@endsection
@endsection

@section('breadcumb-here', 'Dashboard')

@section('content')
	<div class="box box-solid box-primary">
		<div class="box-header">
			<div class="box-title">
				<div class="pull-left">
					Detail {{ $user->name }}
				</div>
			</div>

			<div class="pull-right">
				<a href="{{ route('users.index') }}" class="btn btn-danger"> Go Back</a>
			</div>
		</div>

		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="name">Name</label>
						<input type="text" name="name" id="name" class="form-control" placeholder="Name" autocomplete="off" value="{{ $user->name }}" readonly>
					</div>
					
					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" name="email" id="email" class="form-control" placeholder="Email" autocomplete="off" value="{{ $user->email }}" readonly>
					</div>

					<div class="form-group">
						<label for="address">Address</label>
						<textarea name="address" id="address" class="form-control" placeholder="Address" autocomplete="off" readonly> {{ $user->address }}</textarea>
					</div>

					<div class="form-group">
						<label for="phone">Phone</label>
						<input type="number" name="phone" id="phone" class="form-control" value="{{ $user->phone }}" readonly>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group"> 
						<label for="username">Username</label>
						<input type="text" name="username" id="username" class="form-control" placeholder="Username" autocomplete="off" value="{{ $user->username }}" readonly>
					</div>

					<div class="form-group">
						<label for="email_secondary">Email Secondary</label>
						<input type="email" name="email_secondary" id="email_secondary" class="form-control" placeholder="Email Secondary" autocomplete="off" value="{{ $user->email_secondary }}" readonly>

						<div class="help-block">
							{{ $errors->first('email_secondary') }}
						</div>
					</div>

					<div class="form-group">
						<label for="level">Level</label>
						<input class="form-control" autocomplete="off" value="{{ $user->level }}" readonly>

						<div class="help-block">
							{{ $errors->first('level') }}
						</div>
					</div>

					<div class="form-group">
						<label for="status">Status</label>
						<input class="form-control" autocomplete="off" value="{{ $user->status }}" readonly>
					</div>
				</div>
			</div>
		</div>		
	</div>	
@endsection	