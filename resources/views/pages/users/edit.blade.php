@extends('layouts.global')

@section('title')
	- Update {{ $user->name }}
@endsection

@section('page-header', 'Update User')

@section('page-header-optional')
	Update {{ $user->name }}
@endsection

@section('breadcumb-link')
	{{ route('users.index') }}
	@section('breadcumb-level')
		Users
	@endsection
@endsection

@section('breadcumb-here', 'Update')
	
@section('content')
	<div class="box box-solid box-primary">
		<div class="box-header">
			<div class="box-title">
				<div class="pull-left">
					Update {{ $user->name }}
				</div>
			</div>

			<div class="pull-right">
				<a href="{{ route('users.index') }}" class="btn btn-danger"> Go Back</a>
			</div>
		</div>

		<div class="box-body">
			<form action="{{ route('users.update', $user->id) }}" method="POST" onsubmit="return confirm('Update {{ $user->name }} ? ')" enctype="multipart/form-data">
				@csrf
				<input type="hidden" name="_method" value="PUT">
				
				

			<div class="row">
				<div class="col-md-6">
					<div class="form-group {{ $errors->first('name') ? 'has-error' : '' }}">
						<label for="name">Name</label>
						<input type="text" name="name" id="name" class="form-control" placeholder="Name" autocomplete="off" value="{{ $user->name }}">

						<div class="help-block">
							{{ $errors->first('name') }}
						</div>
					</div>
					
					<div class="form-group {{ $errors->first('email') ? 'has-error' : '' }}">
						<label for="email">Email</label>
						<input type="email" name="email" id="email" class="form-control" placeholder="Email" autocomplete="off" value="{{ $user->email }}">

						<div class="help-block">
							{{ $errors->first('email') }}
						</div>
					</div>

					<div class="form-group {{ $errors->first('address') ? 'has-error' : '' }}">
						<label for="address">Address</label>
						<textarea name="address" id="address" class="form-control" placeholder="Address" autocomplete="off"> {{ $user->address }}</textarea>

						<div class="help-block">
							{{ $errors->first('address') }}
						</div>
					</div>

					<div class="form-group {{ $errors->first('phone') ? 'has-error' : '' }}">
						<label for="phone">Phone</label>
						<input type="number" name="phone" id="phone" class="form-control" value="{{ $user->phone }}">

						<div class="help-block">
							{{ $errors->first('phone') }}
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group {{ $errors->first('username') ? 'has-error' : '' }}">
						<label for="username">Username</label>
						<input type="text" name="username" id="username" class="form-control" placeholder="Username" autocomplete="off" value="{{ $user->username }}">

						<div class="help-block">
							{{ $errors->first('username') }}
						</div>
					</div>

					<div class="form-group {{ $errors->first('email_secondary') ? 'has-error' : '' }}">
						<label for="email_secondary">Email Secondary</label>
						<input type="email" name="email_secondary" id="email_secondary" class="form-control" placeholder="Email Secondary" autocomplete="off" value="{{ $user->email_secondary }}">

						<div class="help-block">
							{{ $errors->first('email_secondary') }}
						</div>
					</div>

					<div class="form-group {{ $errors->first('level') ? 'has-error' : '' }}">
						<label for="level">Level</label>
						<select name="level" id="level" class="form-control">
							<option value="">Choose Level</option>
							<option value="ADMIN" {{ $user->level == 'ADMIN' ? 'selected' : '' }}>ADMIN</option>
							<option value="GUEST" {{ $user->level == 'GUEST' ? 'selected' : '' }}>GUEST</option>
						</select>

						<div class="help-block">
							{{ $errors->first('level') }}
						</div>
					</div>

					<div class="form-group {{ $errors->first('status') ? 'has-error' : '' }}">
						<label for="status">Status</label>
						<select name="status" id="status" class="form-control">
							<option value="">Choose Status</option>
							<option value="1" {{ $user->status == 1 ? 'selected' : '' }}>Active</option>
							<option value="0" {{ $user->status == 0 ? 'selected' : '' }}>Inactive</option>
						</select>

						<div class="help-block">
							{{ $errors->first('status') }}
						</div>
					</div>

					<div class="form-group">
						<label for="image">Image</label>
						<input type="file" name="image" id="image">
					</div>
				</div>
			</div>
		</div>

		<div class="box-footer">
			<button type="submit" name="submit" class="btn btn-primary pull-right">
				Update User
			</button>
		</div>
			</form>				
	</div>	
@endsection