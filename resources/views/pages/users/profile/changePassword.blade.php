@extends('layouts.global')

@section('title')
    - Profile Management - Change Password
@endsection

@section('page-header')
    Profile Management
@endsection

@section('page-header-optional')
    Manage Your Profile Here
@endsection

@section('breadcumb-level')
    @section('breadcumb-link')
        {{ route('user.profile', Auth::user()->id) }}
    @endsection
        My Profile
@endsection

@section('breadcumb-here')
    Change Password 
@endsection

@section('content')

    <div class="box box-solid box-warning">
        <div class="box-header">
            <div class="box-title">
                <div class="pull-left">
                    Change Your Password
                </div>
            </div>

            <div class="pull-right">
                <a href="{{ route('user.profile', $user->id) }}" class="btn btn-default">Go Back</a>
            </div>
        </div>

        <div class="box-body">
            <form action="{{ route('user.password-change', $user->id) }}" method="POST" onsubmit="return confirm('Change Your password?')">
                @csrf
                <input type="hidden" name="_method" value="PUT">
        
                <div class="form-group {{ $errors->first('password') ? 'has-error' : '' }}">
                    <label for="password">New Password</label>
                    <input type="password" name="password" id="password" class="form-control" placeholder="New Password">
                    <div class="help-block">
                        {{ $errors->first('password') }}
                    </div>
                </div>
        
                <div class="form-group {{ $errors->first('password_confirmation') ? 'has-error' : '' }}">
                    <label for="password_confirmation">New Password Confirmation</label>
                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="New Password Confirmation">
                    <div class="help-block">
                        {{ $errors->first('password_confirmation') }}
                    </div>
                </div>

                <button type="submit" class="btn btn-primary btn-block"> Change Your Password</button>
            </form>
        </div>
    </div>

@endsection