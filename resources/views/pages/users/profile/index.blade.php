@extends('layouts.global')

@section('title')
    - Profile Management
@endsection

@section('page-header')
    Profile Management
@endsection

@section('page-header-optional')
    Manage Your Profile Here
@endsection

@section('breadcumb-level')
    @section('breadcumb-link')
        {{ route('user.profile', Auth::user()->id) }}
    @endsection
        My Profile
@endsection

@section('breadcumb-here')
    Dashboard
@endsection

@section('content')
<div class="row">
    <div class="col-md-3">

      <!-- Profile Image -->
      <div class="box box-primary">
        <div class="box-body box-profile">
          <img class="profile-user-img img-responsive img-circle" src="{{ asset('images/users_images/' . $user->image) }}" alt="User profile picture">

          <h3 class="profile-username text-center">{{ $user->name }} {{ $user->username ? '- ' . $user->username : '' }}</h3>

          {{-- Delete Photo Profile --}}    
            <form action="{{ route('user.deletePhotoProfile', $user->id) }}" method="POST" onsubmit="return confirm('Delete Photo Profile?')">
                @csrf
                <input type="hidden" name="_method" value="PUT">
                <button type="submit" class="btn btn-warning btn-block">
                    Delete Your Image 
                </button>
            </form>
            <hr>

            <h3 class="text-center text-danger">Danger Zone</h3>
            <form action="{{ route('user.delete-account', $user->id) }}" method="POST" onsubmit="return confirm('Are You sure want to delete Your account? This cant be revert!')">
                @csrf
                <input type="hidden" name="_method" value="DELETE">
                <button type="submit" class="btn btn-danger btn-block">
                    Delete Your Account
                </button>
            </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->

    <div class="col-md-9">
      <div class="box box-primary box-solid">
        <div class="box-header">
            <div class="box-title">
                <div class="pull-left">
                    Change Your Profile
                </div>
            </div>
        </div>
        
        <div class="box-body">
            {!! Form::open(['route' => ['user.changeProfile', $user->id], 'method' => 'PUT', 'onsubmit' => 'return confirm("Change Your Profile ?")', 'enctype' => 'multipart/form-data' ]) !!}

                <div class="form-group {{ $user->exists ? 'has-success' : '' }} {{ $errors->first('name') ? 'has-error' : '' }}">
                    {{ Form::label('name', 'Name') }}
                    {{ Form::text('name', $user->name, ['class' => 'form-control', 'placeholder' => 'Your Name', 'autocomplete' => 'off']) }}
                    <div class="help-block">
                        {{ $errors->first('name') }}
                    </div>
                </div>

                <div class="form-group {{ $user->exists ? 'has-success' : '' }} {{ $errors->first('username') ? 'has-error' : '' }}">
                    {{ Form::label('username', 'Username[Optional]') }}
                    {{ Form::text('username', $user->username, ['class' => 'form-control', 'placeholder' => 'Your Username', 'autocomplete' => 'off']) }}
                    <div class="help-block">
                        {{ $errors->first('username') }}
                    </div>
                </div>
                
                <div class="form-group {{ $user->exists ? 'has-success' : '' }} {{ $errors->first('email') ? 'has-error' : '' }}">
                    {{ Form::label('email', 'Email') }}
                    {{ Form::text('email', $user->email, ['class' => 'form-control', 'placeholder' => 'Your Email', 'autocomplete' => 'off']) }}
                    <div class="help-block">
                        {{ $errors->first('email') }}
                    </div>
                </div>

                <div class="form-group {{ $user->exists ? 'has-success' : '' }} {{ $errors->first('address') ? 'has-error' : '' }}">
                    {{ Form::label('address', 'Address [Optional]') }}
                    {{ Form::textarea('address', $user->address, ['class' => 'form-control', 'placeholder' => 'Your Address', 'autocomplete' => 'off']) }}
                    <div class="help-block">
                        {{ $errors->first('address') }}
                    </div>
                </div>

                <div class="form-group {{ $user->exists ? 'has-success' : '' }} {{ $errors->first('phone') ? 'has-error' : '' }}">
                    {{ Form::label('phone', 'phone [Optional]') }}
                    {{ Form::number('phone', $user->phone, ['class' => 'form-control', 'placeholder' => 'Your Phone', 'autocomplete' => 'off']) }}
                    <div class="help-block">
                        {{ $errors->first('phone') }}
                    </div>
                </div>

                @if($user->level == "ADMIN")
                    <div class="form-group {{ $user->exists ? 'has-success' : '' }} {{ $errors->first('level') ? 'has-error' : '' }}">
                        {{ Form::label('level') }}
                        @php $levels = ['ADMIN', 'EMPLOYEE', 'GUEST'] @endphp
                        <select name="level" id="level" class="form-control">
                            <option value="">Choose Level</option>
                            @foreach($levels as $level)
                                @if($user->level == $level)
                                    <option value="{{ $level }}" selected>{{ $level }}</option>
                                @else
                                    <option value="{{ $level }}">{{ $level }}</option>
                                @endif
                            @endforeach
                        </select>
                        <div class="help-block">
                            {{ $errors->first('level') }}
                        </div>
                    </div>
                @else 
                    <div class="form-group {{ $user->exists ? 'has-success' : '' }} {{ $errors->first('level') ? 'has-error' : '' }}">
                        {{ Form::label('level', 'Level') }}
                        {{ Form::text('level', $user->level, ['class' => 'form-control', 'placeholder' => 'Level', 'autocomplete' =>' off', 'readonly']) }}
                    </div>
                @endif

                <div class="form-group">
                    {{ Form::label('image', 'Image [Optional]')}}
                    {{ Form::file('image') }}
                </div>

                {{ Form::submit('Change Profile', ['class' => 'btn btn-primary pull-right'])}}
                <a href="{{ route('user.change-password', $user->id) }}" class="btn btn-info btb-block pull-left">Change Password</a>
            {!! Form::close() !!}
        </div>
      </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
@endsection