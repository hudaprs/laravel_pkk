@extends('layouts.global')

@section('title', '- User Management')

@section('page-header', 'User Management')

@section('page-header-optional', 'Create New User')

@section('breadcumb-link')
	{{ route('users.index') }}
	@section('breadcumb-level')
		Users
	@endsection
@endsection

@section('breadcumb-here', 'Create')
	
@section('content')
	<div class="box box-solid box-primary">
		<div class="box-header">
			<div class="box-title">
				<div class="pull-left">
					Create New User
				</div>
			</div>

			<div class="pull-right">
				<a href="{{ route('users.index') }}" class="btn btn-danger"> Go Back</a>
			</div>
		</div>

		<div class="box-body">
			<form action="{{ route('users.store') }}" method="POST" onsubmit="return confirm('Create New User ? ')" enctype="multipart/form-data">
				@csrf
			<div class="row">
				<div class="col-md-6">
					<div class="form-group {{ $errors->first('name') ? 'has-error' : '' }}">
						<label for="name">Name</label>
						<input type="text" name="name" id="name" class="form-control" placeholder="Name" autocomplete="off" value="{{ old('name') }}">

						<div class="help-block">
							{{ $errors->first('name') }}
						</div>
					</div>
					
					<div class="form-group {{ $errors->first('email') ? 'has-error' : '' }}">
						<label for="email">Email</label>
						<input type="email" name="email" id="email" class="form-control" placeholder="Email" autocomplete="off" value="{{ old('email') }}">

						<div class="help-block">
							{{ $errors->first('email') }}
						</div>
					</div>

					<div class="form-group {{ $errors->first('password') ? 'has-error' : '' }}">
						<label for="password">Password</label>
						<input type="password" name="password" id="password" class="form-control" placeholder="Password" autocomplete="off">

						<div class="help-block">
							{{ $errors->first('password') }}
						</div>
					</div>

					<div class="form-group {{ $errors->first('password_confirmation') ? 'has-error' : '' }}">
						<label for="password_confirmation">Password Confirmation</label>
						<input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Password Confirmation" autocomplete="off">

						<div class="help-block">
							{{ $errors->first('password_confirmation') }}
						</div>
					</div>

					<div class="form-group {{ $errors->first('address') ? 'has-error' : '' }}">
						<label for="address">Address</label>
						<textarea name="address" id="address" class="form-control" placeholder="Address">
							{{ old('address') }}
						</textarea>

						<div class="help-block">
							{{ $errors->first('address') }}
						</div>
					</div>

					<div class="form-group {{ $errors->first('phone') ? 'has-error' : '' }}">
						<label for="phone">Phone</label>
						<input type="number" name="phone" id="phone" class="form-control" value="{{ old('phone') }}" placeholder="Phone +62">

						<div class="help-block">
							{{ $errors->first('phone') }}
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group {{ $errors->first('username') ? 'has-error' : '' }}">
						<label for="username">Username</label>
						<input type="text" name="username" id="username" class="form-control" placeholder="Username" autocomplete="off" value="{{ old('username') }}">

						<div class="help-block">
							{{ $errors->first('username') }}
						</div>
					</div>

					<div class="form-group {{ $errors->first('email_secondary') ? 'has-error' : '' }}">
						<label for="email_secondary">Email Secondary</label>
						<input type="email" name="email_secondary" id="email_secondary" class="form-control" placeholder="Email Secondary" autocomplete="off" value="{{ old('email_secondary') }}">

						<div class="help-block">
							{{ $errors->first('email_secondary') }}
						</div>
					</div>

					<div class="form-group {{ $errors->first('level') ? 'has-error' : '' }}">
						<label for="level">Level</label>
						<select name="level" id="level" class="form-control">
							<option value="">Choose Level</option>
							<option value="ADMIN">ADMIN</option>
							<option value="GUEST">GUEST</option>
						</select>

						<div class="help-block">
							{{ $errors->first('level') }}
						</div>
					</div>

					<div class="form-group {{ $errors->first('status') ? 'has-error' : '' }}">
						<label for="status">Status</label>
						<select name="status" id="status" class="form-control">
							<option value="">Choose Status</option>
							<option value="1">Active</option>
							<option value="0">Inactive</option>
						</select>

						<div class="help-block">
							{{ $errors->first('status') }}
						</div>
					</div>

					<div class="form-group">
						<label for="image">Image</label>
						<input type="file" name="image" id="image">
					</div>
				</div>
			</div>
		</div>

		<div class="box-footer">
			<button type="submit" name="submit" class="btn btn-primary pull-right">
				Create User
			</button>
		</div>
			</form>		
@endsection