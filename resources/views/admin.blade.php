@extends('layouts.global')

@section('title', '- Dashboard')

@section('page-header', 'Dashboard')

@section('page-header-optional', 'Mini Mart Dashboard')

@section('breadcumb-link')
	{{ route('dev') }}
	@section('breadcumb-level')
		Admin
	@endsection
@endsection

@section('breadcumb-here', 'Dashboard')

@section('content')
	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="info-box bg-aqua">
			<span class="info-box-icon"><i class="fa fa-shopping-cart"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">Orders</span>
				<span class="info-box-number">{{ $ordersCount }}</span>

				<div class="progress">
				<div class="progress-bar" style="width: {{ $ordersCount }}%"></div>
				</div>
					<span class="progress-description">
				   {{ $ordersCount }} Orders Registered
					</span>
			</div>
			<!-- /.info-box-content -->
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.col -->
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="info-box bg-green">
			<span class="info-box-icon"><i class="fa fa-dropbox"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">Items</span>
				<span class="info-box-number">{{ $itemsCount }}</span>

				<div class="progress">
				<div class="progress-bar" style="width: {{ $itemsCount }}%"></div>
				</div>
					<span class="progress-description">
					{{ $itemsCount }} Item Registered
					</span>
			</div>
			<!-- /.info-box-content -->
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->

  {{-- Chart --}}

  {{-- End Chart --}}


  {{-- Items List --}}
    <div class="box box-solid box-primary">
	    <div class="box-header">
		    <div class="box-title">
			    <div class="text-center">
				  Items List
			    </div>
		    </div>
		
		    <div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
			</div>
	    </div>

	    <div class="box-body">
			
	    </div>
    </div>
  
@endsection

@push('script')
	<script>
		$(function() {
          $('.box-body').load('./dev/load-items-dashboard')

		  // Hide Button Order Now
		  $('#btn-order').hide()
		})
	</script>
@endpush