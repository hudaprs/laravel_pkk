<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Mini Mart | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('plugins/iCheck/square/blue.css') }}">
  {{-- Pace --}}
  <link rel="stylesheet" href="{{ asset('plugins/pace/pace-theme-flash.css') }}">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>Mini Mart</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    @include('layouts.inc.messages')
    {!! Form::open(['route' => 'custom.login', 'method' => 'POST']) !!}
      
      <div class="form-group {{ $errors->first('email') ? 'has-error' : '' }} has-feedback">
        {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'autocomplete' => 'off']) }}
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      
        <div class="help-block">
          {{ $errors->first('email') }}
        </div>
      </div>
      
      <div class="form-group {{ $errors->first('password') ? 'has-error' : ''}} has-feedback">
        {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'autocomplete' => 'off']) }}
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        
        <div class="help-block">
          {{ $errors->first('password') }}
        </div>
      
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input name="remember" id="remember" type="checkbox"> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>

    {!! Form::close() !!}

    {{-- <a href="#">I forgot my password</a><br> --}}
    {{-- <a href="{{ route('register') }}" class="text-center">Register a new membership</a> --}}

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
{{-- Pace JS--}}
<script src="{{ asset('plugins/pace/pace.min.js') }}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });

  // Init Pace
  $(document).ajaxStart(function() {
    Pace.restart();
  });
</script>
</body>
</html>
