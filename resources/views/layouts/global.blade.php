<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Mini Mart @yield('title')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  {{-- Csrf Token --}}
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
  {{-- Style AdminLTE --}}
  <link rel="stylesheet" href="{{ asset('dist/css/skins/skin-blue.min.css') }}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
  <!-- Google Font -->
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  {{-- Pace Style --}}
  <link rel="stylesheet" href="{{ asset('plugins/pace/pace-theme-flash.css') }}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

      <!-- Logo -->
      <a href="{{ url('/dev') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>MM</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Mini</b> Mart</span>
      </a>

      <!-- Header Navbar -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <img src="{{ asset('images/users_images/' . Auth::user()->image) }}" class="user-image"
                  alt="User Image">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs">{{ Auth::user()->name }}</span>
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                  <img src="{{ asset('images/users_images/' . Auth::user()->image) }}" class="img-circle"
                    alt="User Image">
                  <p>
                    {{ Auth::user()->name }} - {{ Auth::user()->level }}
                  </p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="{{ route('user.profile', Auth::user()->id) }}" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="pull-right">
                    <a href="{{ route('logout') }}"
                      onclick="event.preventDefault(); document.getElementById('logout-form').submit()"
                      class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="{{ asset('images/users_images/' . Auth::user()->image) }}" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>{{ Auth::user()->name }}</p>
            <!-- Status -->
            <i class="fa fa-circle text-success"></i> {{ Auth::user()->level }}
          </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">Menu</li>
          <li><a href="{{ url('/dev') }}"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
          
            <li class="treeview">
              <a href="#"><i class="fa fa-gear"></i> <span>Master</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('users.index') }}"><i class="fa fa-users"></i> <span>Users</span></a></li>
                <li><a href="{{ route('items.index') }}"><i class="fa fa-dropbox"></i> <span>Items</span></a></li>
                <li><a href="{{ route('categories.index') }}"><i class="fa fa-circle"></i> <span>Category</span></a></li>
                <li><a href="{{ route('orders.index') }}"><i class="fa fa-shopping-cart"></i> <span>Orders</span></a></li>
              </ul>
            </li>

          <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> <span>Treeview</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
               
                <li><a href="#"><i class="fa fa-book"></i> <span>Menu 1</span></a></li>
              </ul>
            </li>

          <li><a href="{{ route('logout') }}"
              onclick="event.preventDefault(); document.getElementById('logout-form').submit()"><i
                class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
          <form action="{{ route('logout') }}" id="logout-form" method="POST" style="display: none;">
            @csrf
          </form>
          <!-- /.sidebar-menu -->
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          @yield('page-header')
          <small>@yield('page-header-optional')</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="@yield('breadcumb-link')"><i class="fa fa-dashboard"></i>@yield('breadcumb-level')</a></li>
          <li class="active">@yield('breadcumb-here')</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
        
        {{-- Message Session --}}
        @include('layouts.inc.messages')

        @yield('content')

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- To the right -->
      <div class="pull-right hidden-xs">
        Minimarket
      </div>
      <!-- Default to the left -->
      <strong>Copyright &copy; 2019 <a href="#">Minimart Euy</a>.</strong>
    </footer>
  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 3 -->
  <script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
  <!-- DataTables -->
  <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
  {{-- Pace Js --}}
  <script src="{{ asset('plugins/pace/pace.min.js') }}"></script>
  {{-- Init Pace --}}
  <script>
    $(document).ajaxStart(function() {
     Pace.restart();
   });
  </script>
  <!-- Select2 -->
  <script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
  <!-- ChartJS -->
  <script src="{{ asset('bower_components/chart.js/Chart.js') }}"></script>
  <!-- iCheck 1.0.1 -->
  <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
  @stack('script')
</body>

</html>