// Sidenav Script
const sideNav = document.querySelectorAll('.sidenav');
M.Sidenav.init(sideNav);

// Media Script
const slider = document.querySelectorAll('.slider');
M.Slider.init(slider, {
    height: 500,
    indicators: false
});

// Scrollspy
const scroll = document.querySelectorAll('.scrollspy');
M.ScrollSpy.init(scroll);

const materialbox = document.querySelectorAll('.materialboxed');
M.Materialbox.init(materialbox);
